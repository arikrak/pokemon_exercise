# base pokemon class

class Pokemon
  STRENGTH = 10
  DOUBLE_DAMAGE = {fire: :grass, water: :fire, grass: :water}

  attr_accessor :health

  def initialize
    @health = 100
  end

  def attack(opponent)
    opponent.health -= STRENGTH
    opponent.health -= STRENGTH if double_attack?(opponent)
    opponent.health = 0 if opponent.health < 0
  end

  def hp
    @health
  end

  def type
    self.class::TYPE
  end

  def double_attack?(opponent)
     vulnerable_type = DOUBLE_DAMAGE[self.type]
     vulnerable_type == opponent.type
  end

end

# children pokemon classes

class Squirtle < Pokemon
  TYPE = :water
end

class Charmander < Pokemon
  TYPE = :fire
end

class Bulbasaur < Pokemon
  TYPE = :grass
end